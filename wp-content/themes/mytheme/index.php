<?php get_header(); ?>

    <div id="primary" class="content-area inner">

        <?php if (get_post_type() == 'news' && is_active_sidebar( 'sidebar-news' )): ?>
            <div id="content" class="site-content has-sidebar col span_5_6" role="main">
        <?php elseif (get_post_type() == 'event' && is_active_sidebar( 'sidebar-events' )): ?>
            <div id="content" class="site-content has-sidebar col span_5_6" role="main">
        <?php elseif (get_post_type() == 'press' && is_active_sidebar( 'sidebar-press' )): ?>
            <div id="content" class="site-content has-sidebar col span_5_6" role="main">
        <?php elseif (get_post_type() == 'post' && is_active_sidebar( 'sidebar-blog' )): ?>
            <div id="content" class="site-content has-sidebar col span_5_6" role="main">                            
        <?php else: ?>
            <div id="content" class="site-content" role="main">
        <?php endif ?>

        <?php if ( have_posts() ) : ?>
            <?php /* The loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'content', get_post_type() ); ?>
            <?php endwhile; ?>

        <?php else : ?>
            <?php get_template_part( 'home', '' ); ?>
        <?php endif; ?>

        </div><!-- #content -->
        <?php if (get_post_type() == 'post'): ?>
            <?php if ( is_active_sidebar( 'sidebar-blog' ) ) : ?>
                <div id="secondary" class="widget-area col span_1_6" role="complementary">
                    <?php dynamic_sidebar( 'sidebar-blog' ); ?>
                </div><!-- #secondary -->
            <?php endif; ?> 
        <?php endif ?>        
    </div><!-- #primary -->
<?php get_footer(); ?>