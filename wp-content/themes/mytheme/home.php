<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

    <div id="home-content" class="inner">
        <div id="our-mission">
            <div class="title section group">
                <div class="line"><div>&nbsp;</div></div>
                <h2 class="col span_1_5">OUR MISSION</h2>
                <div class="line"><div>&nbsp;</div></div>
            </div>
            <?php
            if (is_active_sidebar('homepage-main-text')) :
                dynamic_sidebar('homepage-main-text');
            endif;
            ?>            
        </div>
        <div id="portraits" class="section group">
            <div class="portrait col span_1_3" id="portrait1">
                <?php
                if (is_active_sidebar('homepage-portrait1')) :
                    dynamic_sidebar('homepage-portrait1');
                endif;
                ?>   
            </div>
            <div class="portrait col span_1_3" id="portrait2">
                <?php
                if (is_active_sidebar('homepage-portrait2')) :
                    dynamic_sidebar('homepage-portrait2');
                endif;
                ?>   
            </div>
            <div class="portrait col span_1_3" id="portrait3">
                <?php
                if (is_active_sidebar('homepage-portrait3')) :
                    dynamic_sidebar('homepage-portrait3');
                endif;
                ?>   
            </div>                      
        </div>
    </div>

<?php get_footer(); ?>    