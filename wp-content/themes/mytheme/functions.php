<?php

function mytheme_setup() {
	register_nav_menu( 'primary', __( 'Navigation Menu', 'mytheme' ) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 150, 150, true );
}
add_action( 'after_setup_theme', 'mytheme_setup' );

function add_class_to_menu_items($output, $args) {
 if( $args->theme_location == 'primary' )
         $output = preg_replace('/class="menu-item/', 'class="col span_1_4 menu-item', $output);
  return $output;
}
add_filter('wp_nav_menu', 'add_class_to_menu_items', 10, 2);

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function mytheme_widgets_init() {

/************* HEADER *********************/
    register_sidebar( array(
      'id'            => 'homepage-social-buttons',
      'name'          => __( 'Homepage - Social buttons', 'mytheme' ),
      'before_widget'  => '',                  
    ) );

    register_sidebar( array(
      'id'            => 'homepage-main-text',
      'name'          => __( 'Homepage - Main Text', 'mytheme' ),
      'before_widget'  => '',                  
    ) );       

    register_sidebar( array(
      'id'            => 'homepage-portrait1',
      'name'          => __( 'Homepage - Portrait 1', 'mytheme' ),
      'before_widget'  => '',                  
    ) );  

    register_sidebar( array(
      'id'            => 'homepage-portrait2',
      'name'          => __( 'Homepage - Portrait 2', 'mytheme' ),
      'before_widget'  => '',                  
    ) );  

    register_sidebar( array(
      'id'            => 'homepage-portrait3',
      'name'          => __( 'Homepage - Portrait 3', 'mytheme' ),
      'before_widget'  => '',                  
    ) );  
            
    register_sidebar( array(
      'id'            => 'footer-copyright',
      'name'          => __( 'Footer - Copyright', 'mytheme' ),
      'before_widget'  => '',                  
    ) );       

    // register_sidebar( array(
    //   'id'            => 'sidebar-blog',
    //   'name'          => __( 'Sidebar - Blog', 'mytheme' ),
    //   'before_widget'  => '',                  
    // ) );  
}
add_action( 'widgets_init', 'mytheme_widgets_init' );

/************* CUSTOM WIDGETS *****************/


/************* SOCIAL LINKS *****************/

class SocialLinkWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Social Link','description=Social Link');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'class' => '', 'link' => '' ) );
			    $class = $instance['class'];
			    $link = $instance['link'];
			?>
			  <p><label for="<?php echo $this->get_field_id('class'); ?>">CSS Class: </label><input class="widefat" id="<?php echo $this->get_field_id('class'); ?>" name="<?php echo $this->get_field_name('class'); ?>" type="text" value="<?php echo attribute_escape($class); ?>"/></p>

			  <p><label for="<?php echo $this->get_field_id('link'); ?>">Link: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['class'] = $new_instance['class'];
		    $instance['link'] = $new_instance['link'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $class = empty($instance['class']) ? '' : apply_filters('widget_title', $instance['class']);
		    $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

		    if (!empty($link))
		      echo "<li class='".$class."'><a href='".$link."' target='_blank'></a></li>";

		    echo $after_widget;
        }

}
register_widget( 'SocialLinkWidget' );

/************* READ MORE LINK *****************/

class ReadMoreWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Main Read More Link','description=Homepage Main Read More Link');
        }

        public function form( $instance ) {
            $instance = wp_parse_args( (array) $instance, array( 'link' => '' ) );
                $link = $instance['link'];
            ?>
              <p><label for="<?php echo $this->get_field_id('link'); ?>">Link: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
            <?php
        }

        public function update( $new_instance, $old_instance ) {
            $instance = $old_instance;
            $instance['link'] = $new_instance['link'];
            return $instance;
        }

        public function widget( $args, $instance ) {
            extract($args, EXTR_SKIP);
         
            echo $before_widget;
            $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

            if (!empty($link))
              echo "<div class='section group read-more'><a href='".$link."'>READ MORE</a></div>";

            echo $after_widget;
        }

}
register_widget( 'ReadMoreWidget' );

/************* SOCIAL LINKS *****************/

class PortraitWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Portrait Box','description=Homepage Portrait Box');
        }

        public function form( $instance ) {
            $instance = wp_parse_args( (array) $instance, array( 'name' => '', 'text' => '', 'link' => '' ) );
                $name = $instance['name'];
                $text = $instance['text'];
                $link = $instance['link'];
            ?>
              <p><label for="<?php echo $this->get_field_id('name'); ?>">Name: </label><input class="widefat" id="<?php echo $this->get_field_id('name'); ?>" name="<?php echo $this->get_field_name('name'); ?>" type="text" value="<?php echo attribute_escape($name); ?>"/></p>
              <p><label for="<?php echo $this->get_field_id('text'); ?>">Text: </label><textarea id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo attribute_escape($text); ?></textarea></p>
              <p><label for="<?php echo $this->get_field_id('link'); ?>">Link: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
            <?php
        }

        public function update( $new_instance, $old_instance ) {
            $instance = $old_instance;
            $instance['name'] = $new_instance['name'];
            $instance['text'] = $new_instance['text'];
            $instance['link'] = $new_instance['link'];
            return $instance;
        }

        public function widget( $args, $instance ) {
            extract($args, EXTR_SKIP);
         
            echo $before_widget;
            $name = empty($instance['name']) ? '' : apply_filters('widget_title', $instance['name']);
            $text = empty($instance['text']) ? '' : apply_filters('widget_title', $instance['text']);
            $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

            if (!empty($link))
              echo "<div><h3 class='title'>".$name."</h3>";
              echo "<div class='textwidget'>".$text."</div>";
              echo "<div class='read-more'><a href='".$link."'>READ MORE</a></div></div>";

            echo $after_widget;
        }

}
register_widget( 'PortraitWidget' );

/************* HELPER FUNCTIONS *****************/

// function html_widget_title( $title ) {
// 	//HTML tag opening/closing brackets
// 	$title = str_replace( '[', '<', $title );
// 	$title = str_replace( ']', '/>', $title );

// 	return $title;
// }
// add_filter( 'widget_title', 'html_widget_title' );

// function html_widget_text( $text ) {
// 	//HTML tag opening/closing brackets
// 	$text = str_replace( '[', '<', $text );
// 	$text = str_replace( ']', '/>', $text );

// 	return $text;
// }
// add_filter( 'widget_text', 'html_widget_text' );

function prefix_nav_menu_args($args = ''){
    $args['container'] = false;
    return $args;
}
add_filter('wp_nav_menu_args', 'prefix_nav_menu_args');