<?php
/*
Template Name: Our Team
*/
?>
<?php get_header(); ?>

<div id="primary" class="content-area inner">

    <div id="content" class="site-content" role="main">

        <article id="home-content" class="inner">

            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>

            <div id="portraits" class="section group">
                <div class="portrait col span_1_3" id="portrait1">
                    <?php
                    if (is_active_sidebar('homepage-portrait1')) :
                        dynamic_sidebar('homepage-portrait1');
                    endif;
                    ?>   
                </div>
                <div class="portrait col span_1_3" id="portrait2">
                    <?php
                    if (is_active_sidebar('homepage-portrait2')) :
                        dynamic_sidebar('homepage-portrait2');
                    endif;
                    ?>   
                </div>
                <div class="portrait col span_1_3" id="portrait3">
                    <?php
                    if (is_active_sidebar('homepage-portrait3')) :
                        dynamic_sidebar('homepage-portrait3');
                    endif;
                    ?>   
                </div>                      
            </div>
        </article>
    </div>
</div>            

<?php get_footer(); ?>    