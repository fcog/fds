    <footer id="footer" class="inner">
        <div id="footer-bot">
            <?php
            if (is_active_sidebar('footer-copyright')) :
                dynamic_sidebar('footer-copyright');
            endif;
            ?>              
        </div>
    </footer>
</div>
<script type="text/javascript">
var ww = document.body.clientWidth;

jQuery(function() {
  jQuery("#menu-trigger").click(function(e) {
      e.preventDefault();
      jQuery("#main-menu").toggle();
  });

  adjustMenu();	
});	

jQuery(window).bind('resize orientationchange', function() {
  ww = document.body.clientWidth;
  adjustMenu();
});

var adjustMenu = function() {
  if (ww <= 480) {
  	jQuery("#main-menu").hide();
  	jQuery("#menu-trigger").show();
  } 
  else {
    jQuery("#main-menu").show();
    jQuery("#menu-trigger").hide();
  }
}
</script>
</body>
</html>