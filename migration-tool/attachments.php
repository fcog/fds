<html>
<head>
	<link rel="stylesheet" type="text/css" href="styles.css" />
</head>
<body>
<h1>Solve for Attachments</h1>
<?php
/**
 * Script for updating WP instance when migrating among domains or paths
 * @author felipe@inqbation.com
 * @copyright Lufeceba 2008
 * @version 1.2
 */

	/** check if wp-config file present **/
	$form_msg = "";
	if (file_exists("../wp-config.php")) {
		include("../wp-config.php");
		$conn['dbname'] = DB_NAME;
		$conn['dbuser'] = DB_USER;
		$conn['dbpwd'] = DB_PASSWORD;
		$conn['server'] = DB_HOST;
		$settings['table_prefix'] = $table_prefix; //taken from file
	} else {
		$form_msg = "File wp-config.php not present, you should configure the DB connection params";

	}

	if (file_exists("../../wp-config.php")) {
		//echo "Source config file found!";
		//include("../../wp-config.php");
//		$source['dbname'] = DB_NAME;
//		$source['dbuser'] = DB_USER;
//		$source['dbpwd'] = DB_PASSWORD;
//		$source['server'] = DB_HOST;
//		$source['table_prefix'] = $table_prefix; //taken from file
		$source['dbname'] = 'fedscoop_isl';
		$source['dbuser'] = 'fedscoop_isl';
		$source['dbpwd'] = 'dsjkgdhs0987KJHJHRb';
		$source['server'] = 'localhost';
		$source['table_prefix'] = 'wp_'; //taken from file
	} else {
		$form_msg = "File Source wp-config.php not present, you should configure the DB connection params";
	}

	/** check if form was submitted **/
	if( !empty($_POST) ) {
		extract($_POST);
		if(!empty($conn_dbname)) {
			$conn['dbname'] = $conn_dbname;
			$conn['dbuser'] = $conn_dbuser;
			$conn['dbpwd'] = $conn_password;
			$conn['server'] = $conn_hostname;
			$settings['table_prefix'] = $table_prefix;
		}
	}

//	some general settings
/*	$settings["olddomain"] = trim($old_domain);
	$settings["newdomain"] = trim($new_domain);
	$settings["checkroot"] = $check_root;  // checks entries without http://localhost/ string, but the remaining string
	$settings["oldpath"] = trim(stripslashes($old_path));
	$settings["newpath"] = trim($new_path)?trim(stripslashes($new_path)):dirname(dirname( __FILE__ ));  // use this file's path or
//	$settings["newpath"] = 'F:\webdevelop\htdocs\clients\sb';  // set it manually*/
	$settings["debug"] = $debug_mode;  // set to false so that the update queries are really executed

	// load the form
	include("form-attachments.php");

	if (!empty($run_queries)) {
?>
	<fieldset>
		<legend>Results</legend>
<?php
		$queries['post'] = "SELECT ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count FROM {$source['table_prefix']}posts WHERE post_type = 'attachment' ORDER BY ID ASC";


	// 	main queries - don't change them
	/*	$queries["options"] = "UPDATE {$settings['table_prefix']}options SET option_value = replace(option_value, '{$settings["olddomain"]}', '{$settings["newdomain"]}') WHERE option_name='home' OR option_name = 'siteurl'";
		$queries["p_guid"] = "UPDATE {$settings['table_prefix']}posts SET guid = replace(guid, '{$settings["olddomain"]}','{$settings["newdomain"]}')";
		$queries["p_content1"] = "UPDATE {$settings['table_prefix']}posts SET post_content = replace(post_content, '{$settings["olddomain"]}', '{$settings["newdomain"]}')";
	*/

	// Let's begin
		if ( $settings["debug"] ) {
			echo "<p style='background-color: #ddd'>Debug is true, did you already configured everything??</p>";
		} else {
			echo "<p style='background-color: #666; font-color:#fff'>Let's run the scripts..... </p>";
		}

		echo "connecting to the source database server<br/>";
		$source_link = mysql_connect( $source["server"], $source["dbuser"], $source["dbpwd"] ) or die( "Unable to connect to the Source DB Engine" );
		echo "Changing the Source database<br />";
		mysql_select_db( $source["dbname"], $source_link ) or die( "Unable to access the database, check the privileges" );


		// connect to the target DB
		$link = mysql_connect( $conn["server"], $conn["dbuser"], $conn["dbpwd"] ) or die( "Unable to connect to the DB Engine" );
		echo "Changing the Target database<br />";
		mysql_select_db( $conn["dbname"], $link ) or die( "Unable to access the database, check the privileges" );

		echo "Checking magic_quotes_gpc... ";
		echo ini_get("magic_quotes_gpc");
		echo "<br />";


		// select the posts
		echo "Selecting: ". $queries['post'].'<br/>';
		$res = mysql_query( $queries["post"], $source_link ) or die ( "Unable to get postmeta records" );

		if ( $res ) {
			$i = 1;
			while ( $row = mysql_fetch_assoc($res) ) {
				$insert_query = "INSERT INTO {$settings['table_prefix']}posts (`post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`)
VALUES ( 1, '{$row['post_date']}', '{$row['post_date_gmt']}', '{$row['post_content']}', '{$row['post_title']}', '{$row['post_excerpt']}', '{$row['post_status']}', '{$row['comment_status']}', '{$row['ping_status']}', '', '{$row['post_name']}', '{$row['to_ping']}', '{$row['pinged']}', '{$row['post_modified']}', '{$row['post_modified_gmt']}', '{$row['post_content_filtered']}', {$row['post_parent']}, '{$row['guid']}', {$row['menu_order']}, '{$row['post_type']}', '{$row['post_mime_type']}', {$row['comment_count']})";

				echo $i++ .". To execute: ". $insert_query.'<br/>';
				$sposts[] = $row['ID'];
				if ( !$settings["debug"] ) {
					$insres = mysql_query($insert_query, $link);
					if (mysql_affected_rows($link)) {
						$newpid = mysql_insert_id($link);
						echo "POST: inserted post with ID $newpid/{$row['ID']}<br />";
						$posts_ids[$row['ID']] = $newpid;
					} else {
						echo "POST: ERROR while inserting post ID {$row['ID']}<br />";
					}
				}
			}

			// Now, deal with attachments meta
			$queries['postmeta'] = "SELECT meta_id, post_id, meta_key, meta_value FROM {$settings['table_prefix']}postmeta WHERE post_id IN (". implode(', ', $sposts) .") ORDER BY post_id ASC";

			echo "Selecting: ". $queries['postmeta'].'<br/>';
			$res = mysql_query($queries['postmeta'], $source_link);
			if ($res) {
				$i = 1;
				while ( $row = mysql_fetch_assoc($res) ) {
					$metavalue = mysql_real_escape_string( $row["meta_value"], $link );
					$insert_query = "INSERT INTO {$settings['table_prefix']}postmeta (post_id, meta_key, meta_value) VALUES ({$posts_ids[$row['post_id']]}, '{$row['meta_key']}', '$metavalue')";

					echo $i++ .". To execute: ". $insert_query.'<br/>';
					if ( !$settings["debug"] ) {
						$insres = mysql_query($insert_query, $link);
						if (mysql_affected_rows($link)) {
							$newpid = mysql_insert_id($link);
							echo "POST: inserted post-meta with ID $newpid/{$row['ID']} for post {$posts_ids[$row['post_id']]}<br />";
							$posts_ids[$row['ID']] = $newpid;
							$sposts[] = $row['ID'];
						} else {
							echo "POST: ERROR while inserting post-meta ID {$row['ID']} for post {$posts_ids[$row['post_id']]}<br />";
						}
					}
				}
			}

		}


		echo "done!";
	}
?>
	</fieldset>
</body>
</html>
